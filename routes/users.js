const express = require('express');
const router = express.Router();
const User = require('../models/User');
const bcrypt = require('bcryptjs');
const passport = require('passport');

const {ensureAuthenticated} = require('../config/auth');


/// display a registration form
router.get('/register', (req, res) => res.render("users/signup"));

//display a login form
router.get('/login', (req, res) => res.render('users/login'));


router.get('/dashboard', ensureAuthenticated, (req, res) => res.render('users/dashboard'));

//submit register
router.post('/register', (req, res) => {
         
        let errors = [];
        let { email, password, confirm_password } = req.body;
        //check required fields
        if (!email || !password || !confirm_password) {
                errors.push({
                        msg: "Merci de remplir tous les champs",
                })
        }
        // check password match
        if (password !== confirm_password) {
                errors.push({
                        msg: "les mots de passe ne correspondent pas",
                })
        }
        // password must be > 6 caracters
        if (password.length < 6) {
                errors.push({
                        msg: "le mot de passe doit avoir au minimum 6 caractères"
                })
        }
      
        //check if errors
        if (errors.length > 0) {
                res.render('users/signup', {
                        errors: errors,
                        email: email,
                        password: password,
                        confirm_password: confirm_password
                })
        } else {
                User.findOne({ where: { email: email } })
                        .then(user => {
                                if (user) {
                                        errors.push({ msg: "l'utilisateur exciste déjà dans la base" })
                                        res.render('users/signup', {
                                                errors: errors,
                                                email: email,
                                                password: password,
                                                confirm_password: confirm_password
                                        })
                                } else {
                                        // we create our new user
                                        const newUser = new User({
                                                email,
                                                password,
                                                role: "member",
                                                status: 0
                                        })
                                     
                                        User.createUser(newUser, function (err, user) {
                                                if (err) throw err
                                        })
                                        req.flash('success_msg', "Vous êtes maintenant enregistré vous pouvez vous connecter")
                                        res.redirect('login')
                                }
                        }).catch(err => {
                                console.log(err)
                        })
        }

});

// submit login
router.post('/login', (req, res, next) => {
        passport.authenticate('local', {
                successRedirect: '/dashboard',
                failureRedirect: '/users/login',
                failureFlash: true
        })(req, res, next);
});


// logout
router.get('/logout',  (req, res)=> {
        req.logout();
        req.flash('success_msg', "Vous êtes maintenant déconnecté");
        res.redirect('/users/login');
});

module.exports = router;