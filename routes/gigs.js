const express = require('express')
const router = express.Router()
const db  = require('../config/database')
const Gig = require('../models/Gig')

const Sequelize = require('sequelize')
const Op = Sequelize.Op

// Pagination
const paginate = require('express-paginate');


// display add gig form
router.get('/add',(req,res)=>res.render('add'))

// add a gig
router.post('/add',(req,res)=>{
    let {title,technologies,budget,description,contact_email} = req.body

    let errors = []

    if(!title){
        errors.push({text:"Please add a title"})
    }
    
    if(!technologies){
        errors.push({text:"Please add a technology"})
    }
    
    if(!description){
        errors.push({text:"Please add a description"})
    }
    
    if(!budget){
        errors.push({text:"Please add a budget"})
    }
    
    if(!contact_email){
        errors.push({text:"Please add a email"})
    }

    //check for errros
    if(errors.length>0){
        res.render('add',{
            errors,
            title,
            budget,
            technologies,
            description,
            contact_email
        })
    }else{
        //check if budget is empty
        if(!budget){
            budget = "Unknow"
        }else{
            budget = `$${budget}`
        }
        // lowercase technologies and remove space after comma
        technologies = technologies.toLowerCase().replace('/, /g', '/,/')
        //insert into table
        Gig.create({
            title,
            technologies,
            budget,
            description,
            contact_email
        })
        .then(gig=>res.redirect('/gigs'))
        .catch(err=>(console.log(err)))
    }

    
})
// get gigs list
router.get('/', (req, res,next) =>
    Gig.findAndCountAll({limit: req.query.limit, offset: req.skip})
        .then(results => {
            const itemCount = results.count;
            const pageCount = Math.ceil(results.count / req.query.limit);
          
            res.render('gigs',{
                gigs: results.rows, 
                pageCount,
                itemCount,
                pages: paginate.getArrayPages(req)(3, pageCount, req.query.page)
            })
        })
        .catch(error=>console.log(error))
)

// seach for a gigs
router.get('/search',(req,res,next)=>{
    
   let {term } = req.query

   term = term.toLowerCase()
   Gig.findAndCountAll({limit: req.query.limit, offset: req.skip,where : {technologies:{[Op.like]:'%'+ term +'%'}}})
   .then((results)=>{
        const itemCount = results.count;
        const pageCount = Math.ceil(results.count / req.query.limit);
        res.render('gigs',{
            gigs:results.rows, 
            pageCount,
            itemCount,
            pages: paginate.getArrayPages(req)(3, pageCount, req.query.page),
            term
        })
   })
   .catch(err=>console.log("Error " + err))
})


module.exports  = router