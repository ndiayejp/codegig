const sequelize = require('sequelize');
const db = require('../config/database');
const bcrypt = require('bcryptjs');

const User = db.define('user', {
    id: {
        autoIncrement: true,
        primaryKey: true,
        type: sequelize.INTEGER
    },
    email: {
        type: sequelize.STRING,
        validate: {
                isEmail: true
            }
    },
    password: {
        type: sequelize.STRING,
         allowNull: false
    },
    status: {
            type: sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
    },
    role:{
        type: sequelize.STRING
    }
})

module.exports = User

// hash password and save user
module.exports.createUser = function (newUser, callback) {
    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(newUser.password, salt, function (err, hash) { 
            if (err) throw err
            newUser.password = hash
            newUser.save(callback)
        });
    });
}