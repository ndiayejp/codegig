const express = require("express")
const exphbs = require("express-handlebars")
const bodyParser = require("body-parser")
const passport = require('passport');
const paginate = require('express-paginate')
const path = require('path')

const flash = require('connect-flash')
const session = require('express-session')

// Init App
const app = express();

 // Database
const db = require('./config/database')
 
//Test connexion
db.authenticate()
    .then(() => console.log("database connexion"))
    .catch(error=>console.log('Error'+error))



// Passport Config
require('./config/passport')(passport);

// View Engine
app.engine('handlebars', exphbs({ defaultLayout: 'main' }))
app.set('view engine','handlebars')

// BodyParser Middleware for form request
app.use(bodyParser.urlencoded({extended:true}))

// set static folder
app.use(express.static(path.join(__dirname, 'public')))

// keep this before all routes that will use pagination
app.use(paginate.middleware(3, 50));

// Connect Flash
app.use(flash());

// Express session
app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}))


// Passport Middleware
app.use(passport.initialize());
app.use(passport.session());


// Global Vars
app.use(function (req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
});



// routes
app.use('/gigs', require('./routes/gigs'))
app.use('/users', require('./routes/users'))

app.get('/', (req, res) => res.render('index', {
    layout: 'landing'
}));

app.get('/dashboard', (req, res) => res.render('dashboard', {
    layout: 'dashboard'
}));

// Set Port
const PORT = process.env.PORT || 5000;

app.listen(PORT, console.log(`server start on port ${PORT}`));

